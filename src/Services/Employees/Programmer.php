<?php

namespace App\Services\Employees;

class Programmer implements EmployeeInterface
{
    private const ACTIONS = [
        'writeCode'=>'write code',
        'testCode'=>'test code',
        'talkWithManager'=>'talk with manager',
    ];

    public function execute(): array
    {
        return array_values(self::ACTIONS);
    }

    public function can(string $action): bool
    {
        return array_key_exists($action,self::ACTIONS) ?? false;
    }
}