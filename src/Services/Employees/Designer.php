<?php

namespace App\Services\Employees;

class Designer implements EmployeeInterface
{
    private const ACTIONS = [
        'draw'=>'draw',
        'talkWithManager'=>'talk with manager',
    ];

    public function execute(): array
    {
        return array_values(self::ACTIONS);
    }

    public function can(string $action): bool
    {
        return array_key_exists($action,self::ACTIONS) ?? false;
    }
}