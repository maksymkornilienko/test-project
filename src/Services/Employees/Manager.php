<?php

namespace App\Services\Employees;

class Manager implements EmployeeInterface
{
    private const ACTIONS = [
        'setTask'=>'set task',
    ];

    public function execute(): array
    {
        return array_values(self::ACTIONS);
    }

    public function can(string $action): bool
    {
        return array_key_exists($action,self::ACTIONS) ?? false;
    }
}