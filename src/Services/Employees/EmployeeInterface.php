<?php

namespace App\Services\Employees;

interface EmployeeInterface
{
    public function execute() :array;
    public function can(string $action) :bool;
}