<?php

namespace App\Services\Employees;

class Qa implements EmployeeInterface
{
    private const ACTIONS = [
        'talkWithManager'=>'talk with manager',
        'testCode'=>'test code',
        'setTask'=>'set task',
    ];

    public function execute(): array
    {
        return array_values(self::ACTIONS);
    }

    public function can(string $action): bool
    {
        return array_key_exists($action,self::ACTIONS) ?? false;
    }
}