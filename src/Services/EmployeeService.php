<?php

namespace App\Services;

use App\Services\Employees\EmployeeInterface;

class EmployeeService
{
    public const EMPLOYEE_LIST=[
        'programmer'=> Employees\Programmer::class,
        'designer'=> Employees\Designer::class,
        'qa'=> Employees\Qa::class,
        'manager'=> Employees\Manager::class,
    ];

    public function process(string $name) :array
    {
        return $this->getEmployee($name)->execute();
    }

    public function can(string $name,string $action) :bool
    {
        return $this->getEmployee($name)->can($action);
    }

    private function getEmployee(string $name) :EmployeeInterface
    {
        $class = self::EMPLOYEE_LIST[$name];
        return new $class();
    }
}