<?php

namespace App\Command;

use App\Services\EmployeeService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Completion\CompletionInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'regex',
    description: 'Get employee actions.',
    hidden: false,
    aliases: ['Regex']
)]
class RegexCommand extends \Symfony\Component\Console\Command\Command
{

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $string = '<div id="form-holder">
    <div id="main-box">
      <form action="" id="calculate-loan-form">
        <div class="form-item">
          <label for="loan-amount"><b>Enter Loan Amount</b></label>
          <input type="number" step="any" min="0" name="loan-amount" id="loan-amount" oninput="countLoan(event)" placeholder="xxxxx" class="input-item text-right" required>
        </div>
        <div class="form-item">
          <label for="loan-month"><b>Month</b></label>
          <input type="number" step="any" min="1" name="loan-years" id="loan-month" oninput="countLoan(event)"  placeholder="xx" class="input-item text-right" required>
        </div>
      </form>
      <table id="result">
      <img src="http://img01.ibnlive.in/ibnlive/uploads/2015/11/Videocon-Delite.gif" width="90" height="62">
      <img src="http://img01.ibnlive.in/ibnlive/uploads/2015/11/Videocon-Delite.gif" width="90" height="62">
      <img src="http://img01.ibnlive.in/ibnlive/uploads/2015/11/Videocon-Delite.gif" width="90" height="62">
      <img src="http://img01.ibnlive.in/ibnlive/uploads/2015/11/Videocon-Delite.gif" width="90" height="62">
      <img src="http://img01.ibnlive.in/ibnlive/uploads/2015/11/Videocon-Delite.gif" width="90" height="62">
      <img src="http://img01.ibnlive.in/ibnlive/uploads/2015/11/Videocon-Delite.gif" width="90" height="62">
        <colgroup>
          <col width="50%">
          <col width="50%">
        </colgroup>
        <tbody>
        <tr>
          <td class=""><b>Sum pay</b></td>
          <td class="text-right" id="sum-pay"></td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>';

        $pattern = '/(<img.+src=)([\'"]([^\'"?]+))([\'"?])(.*)>/';
        $hash=rand(0, 9999999);
        $replacement = "$1$2?v=$hash',$5";
        $output->writeln(preg_replace($pattern, $replacement, $string));

        return Command::SUCCESS;
    }
}