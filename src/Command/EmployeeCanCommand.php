<?php

namespace App\Command;

use App\Services\EmployeeService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Completion\CompletionInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'can',
    description: 'Get employee actions.',
    hidden: false,
    aliases: ['can']
)]
class EmployeeCanCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(private \App\Services\EmployeeService $employeeService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                'role',
                InputArgument::REQUIRED,
                'Enter one of roles ( '.implode(', ', EmployeeService::EMPLOYEE_LIST).' )',
            )->addArgument(
                'action',
                InputArgument::REQUIRED,
                'Enter action',
            );
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {

        try {
            $can = $this->employeeService->can($input->getArgument('role'), $input->getArgument('action'));
        } catch (\Exception $exception){
            $output->writeln('Undefined role: '.$input->getArgument('role'));
            return Command::FAILURE;
        }

        $output->writeln([
            'I am '.$input->getArgument('role'),
            '============',
            '',
        ]);
        $output->writeln($can ? 'I can: '.$input->getArgument('action'):'I can\'t: '.$input->getArgument('action'));

        return Command::SUCCESS;
    }
}