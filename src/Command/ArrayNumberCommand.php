<?php

namespace App\Command;

use App\Services\EmployeeService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Completion\CompletionInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'ArrayNumber',
    description: '',
    hidden: false,
    aliases: ['ArrayNumber']
)]
class ArrayNumberCommand extends \Symfony\Component\Console\Command\Command
{
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $xor = 0;
        $nums='121303948';
        $nums = str_split($nums);
        $newnums=[];
		foreach ($nums as $num) {
            $xor ^= (1 << $num);
        }
        foreach ($nums as $num) {
            if (($xor & (1 << $num)) != 0)
			{
                $newnums[]=$num;
				$xor ^= (1 << $num);	// to avoid printing duplicates
			}
		}
         $output->writeln($newnums);
        return Command::SUCCESS;
    }
}