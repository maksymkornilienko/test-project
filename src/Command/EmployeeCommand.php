<?php

namespace App\Command;

use App\Services\EmployeeService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Completion\CompletionInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'user',
    description: 'Get employee actions.',
    hidden: false,
    aliases: ['user']
)]
class EmployeeCommand extends \Symfony\Component\Console\Command\Command
{
    public function __construct(private \App\Services\EmployeeService $employeeService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                'role',
                InputArgument::REQUIRED,
                'Enter one of roles ( '.implode(', ', EmployeeService::EMPLOYEE_LIST).' )',
            );
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {

        try {
            $actions = $this->employeeService->process($input->getArgument('role'));
        } catch (\Exception $exception){
            $output->writeln('Undefined role: '.$input->getArgument('role'));
            return Command::FAILURE;
        }

        $output->writeln([
            'I am '.$input->getArgument('role'),
            '============',
            '',
        ]);
        $output->writeln('I can: '.implode(', ', $actions));

        return Command::SUCCESS;
    }
}