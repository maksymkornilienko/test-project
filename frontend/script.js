window.onload = function(e) {
    countLoan(e);
}
function countLoan(e){
    e.preventDefault();
    const principalAmount = document.getElementById('loan-amount').value;
    const PayableMonths = document.getElementById('loan-month').value;
    let monthlyPay = 0;
    monthlyPay = parseFloat(principalAmount) / parseFloat(PayableMonths);
    if (isNaN(monthlyPay)){
        monthlyPay = 0;
    }
    setTimeout(() => {
        document.getElementById('sum-pay').textContent = parseFloat(monthlyPay).toLocaleString("en-US", { style: "decimal", maximumFractionDigits: 2 });
    }, 500);

}