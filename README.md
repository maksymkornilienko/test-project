Функционал используемый при разработки: 
1) symfony 6.1; 
2) openserver; 
3) mysql 5.7; 
4) php 8.1.

Перед тестированием выполнить команды:
1) composer install
2) Если нужно поменять конфигурацию БД она находится в .env
3) php bin/console doctrine:database:create
4) php bin/console doctrine:migrations:migrate

Task 1
Запрос для бд согласно задачи
SELECT g.name,c.name,count(g.country_id) as count_games_by_country from game g inner join country c on g.country_id = c.id group by g.country_id,g.name having count_games_by_country>=3

Task 2
Сделана консольная команда ArrayNumberCommand.php для запуска необходимо запустить команду
php bin/console ArrayNumber

Task 3
Сделана консольная команда RegexCommand.php для запуска необходимо запустить команду
php bin/console regex

Task 4
Находится в папке frontend запустить html в браузере

Task 5
Сделано две консольные команды EmployeeCommand.php и EmployeeCanCommand.php для запуска необходимо запустить команды
php bin/console user [user_role]
php bin/console can [user_role] [action]